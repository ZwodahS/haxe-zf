package zf.animations;

interface Alphable {
	public var alpha(get, set): Float;
}
