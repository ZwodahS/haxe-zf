package zf.animations;

interface Rotatable {
	public var rotation(get, set): Float;
}
