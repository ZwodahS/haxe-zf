package zf.animations;

import zf.Point2f;

interface Positionable {
	public var x(get, set): Float;
	public var y(get, set): Float;
}
